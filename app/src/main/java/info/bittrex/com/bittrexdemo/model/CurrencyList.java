package info.bittrex.com.bittrexdemo.model;

/**
 * Created by arvind.kumar on 11/30/2017.
 */

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CurrencyList {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("Result")
    @Expose
    private List<CurrencyResult> currencyResult = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<CurrencyResult> getCurrencyResult() {
        return currencyResult;
    }

    public void setCurrencyResult(List<CurrencyResult> currencyResult) {
        this.currencyResult = currencyResult;
    }

}
