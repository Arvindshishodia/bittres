package info.bittrex.com.bittrexdemo;


import info.bittrex.com.bittrexdemo.settings.ApplicationSettings;

public class Application extends android.app.Application {
    @Override
    public void onCreate() {
        super.onCreate();

        ApplicationSettings.initialize(this);
    }
}
