package info.bittrex.com.bittrexdemo.model;

/**
 * Created by arvind.kumar on 11/30/2017.
 */


import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SummaryResponse {

        @SerializedName("success")
        @Expose
        private Boolean success;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("result")
        @Expose
        private List<SummaryResult> result = null;

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<SummaryResult> getResult() {
            return result;
        }

        public void setResult(List<SummaryResult> result) {
            this.result = result;
        }

    }
