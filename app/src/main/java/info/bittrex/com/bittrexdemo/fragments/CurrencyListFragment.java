package info.bittrex.com.bittrexdemo.fragments;


import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import info.bittrex.com.bittrexdemo.Connection.AsyncResponse;
import info.bittrex.com.bittrexdemo.CurrencyActivity;
import info.bittrex.com.bittrexdemo.R;
import info.bittrex.com.bittrexdemo.adapters.CurrencyAdapter;
import info.bittrex.com.bittrexdemo.model.SummaryResponse;
import info.bittrex.com.bittrexdemo.model.SummaryResult;
import info.bittrex.com.bittrexdemo.services.CurrencyService;
import info.bittrex.com.bittrexdemo.utils.SharedPreferenceHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class CurrencyListFragment extends Fragment implements AsyncResponse {
    CurrencyService currencyService;
    boolean mBound = false;
    private RecyclerView recycle_currency;
    SummaryResponse summaryResponse = new SummaryResponse();
    CurrencyAdapter currencyAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    private ProgressBar progress;
    private boolean isFirst = false;
    private Gson gson;
    private List<SummaryResult> summaryResults;
    private int currentVisiblePos = 0;

    public CurrencyListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_currency_list, container, false);
        recycle_currency = (RecyclerView) view.findViewById(R.id.recycle_currency);
        progress = (ProgressBar) view.findViewById(R.id.progress);
        isFirst = true;
        summaryResults = new ArrayList<>();

        mLayoutManager = new LinearLayoutManager(getActivity());
        currencyAdapter = new CurrencyAdapter(getActivity(), summaryResults);
        recycle_currency.setLayoutManager(mLayoutManager);
        recycle_currency.setAdapter(currencyAdapter);
        gson = new Gson();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        CurrencyService.asyncResponse = this;
        Intent intent = new Intent(getActivity(), CurrencyService.class);
        getActivity().bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void processFinished(String data) {
       // currentVisiblePos = ((LinearLayoutManager) recycle_currency.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
        SharedPreferenceHelper.with(getActivity()).addString("currencylist",data);
        summaryResponse = gson.fromJson(data, SummaryResponse.class);
        summaryResults.clear();
        summaryResults.addAll(summaryResponse.getResult());

        currencyAdapter.notifyDataSetChanged();
       // ((LinearLayoutManager) recycle_currency.getLayoutManager()).scrollToPosition(currentVisiblePos);
      //  currentVisiblePos = 0;
        progress.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Intent intent = new Intent(getActivity(), CurrencyService.class);
        getActivity().stopService(intent);
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            CurrencyService.LocalBinder binder = (CurrencyService.LocalBinder) service;
            currencyService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
        }
    };
}
