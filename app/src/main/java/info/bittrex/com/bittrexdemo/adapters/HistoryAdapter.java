package info.bittrex.com.bittrexdemo.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.corycharlton.bittrexapi.model.OrderHistory;

import java.util.ArrayList;

import info.bittrex.com.bittrexdemo.R;
import info.bittrex.com.bittrexdemo.model.SummaryResponse;
import info.bittrex.com.bittrexdemo.model.SummaryResult;

/**
 * Created by arvind.kumar on 11/30/2017.
 */

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {
    Context mContext;
    ArrayList<OrderHistory> orderHistory;

    public HistoryAdapter(Context context,ArrayList<OrderHistory> orderHistory) {
        this.mContext = context;
        this.orderHistory = orderHistory;

    }

    @Override
    public HistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.history_items, parent, false);
        HistoryAdapter.ViewHolder viewHolder = new HistoryAdapter.ViewHolder(view);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(HistoryAdapter.ViewHolder holder, int position) {
       /*SummaryResult result= currencyList.getCurrencyResult().get(position);
        holder.txt_shortDesc.setText(result.getMarketName());
        holder.txt_Desc.setText("Vol :"+result.getBaseVolume());
        holder.txt_changePer.setText("Prev Day "+result.getPrevDay());
        holder.txt_changeRs.setText("Last "+result.getLast());*/

        holder.txtMarket.setText(orderHistory.get(position).exchange());
        holder.txtType.setText(orderHistory.get(position).orderType());
        holder.txtClosed.setText(""+orderHistory.get(position).closed());
        //holder.txtOpend.setText(""+orderHistory.get(position).closed());
        holder.txtMarket.setText(orderHistory.get(position).exchange());
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemCount() {
        return orderHistory.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txtClosed;
        private TextView txtOpend;
        private TextView txtMarket;
        private TextView txtType;
        private TextView txtBid;
        private TextView txtUnitfilled;
        private TextView txtUnittotle;
        private TextView txtActualrate;
        private TextView txtProceeds;

        public ViewHolder(View itemView) {
            super(itemView);

            txtClosed = (TextView) itemView.findViewById(R.id.txt_closed);
            txtOpend = (TextView) itemView.findViewById(R.id.txt_opend);
            txtMarket = (TextView) itemView.findViewById(R.id.txt_market);
            txtType = (TextView) itemView.findViewById(R.id.txt_type);
            txtBid = (TextView) itemView.findViewById(R.id.txt_bid);
            txtUnitfilled = (TextView) itemView.findViewById(R.id.txt_unitfilled);
            txtUnittotle = (TextView) itemView.findViewById(R.id.txt_unittotle);
            txtActualrate = (TextView) itemView.findViewById(R.id.txt_actualrate);
            txtProceeds = (TextView) itemView.findViewById(R.id.txt_proceeds);


        }


    }
}
