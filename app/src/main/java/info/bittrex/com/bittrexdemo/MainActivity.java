package info.bittrex.com.bittrexdemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.corycharlton.bittrexapi.internal.util.StringUtils;

import info.bittrex.com.bittrexdemo.settings.ApplicationSettings;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        updateAuthenticationSettings();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        Intent intent=new Intent(MainActivity.this,HomeActivity.class);
        startActivity(intent);
        finish();
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this,CurrencyActivity.class);
                startActivity(intent);
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                  //      .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateAuthenticationSettings() {
        String KEY_KEY = "e663bd7c5c454a28b6b4d4b147de9d4b";
         String KEY_SECRET = "66c0014fbe4949a68a54a5d40325c3bf";

        if (!StringUtils.isNullOrWhiteSpace(KEY_KEY) && !StringUtils.isNullOrWhiteSpace(KEY_SECRET)) {
            ApplicationSettings.instance().setKey(KEY_KEY);
            ApplicationSettings.instance().setSecret(KEY_SECRET);
        }
    }
}
