package info.bittrex.com.bittrexdemo.model;


import com.google.gson.annotations.SerializedName;

public class ResultItem{

	@SerializedName("Condition")
	private Object condition;

	@SerializedName("Commission")
	private double commission;

	@SerializedName("Quantity")
	private double quantity;

	@SerializedName("IsConditional")
	private boolean isConditional;

	@SerializedName("TimeStamp")
	private String timeStamp;

	@SerializedName("ConditionTarget")
	private Object conditionTarget;

	@SerializedName("Exchange")
	private String exchange;

	@SerializedName("OrderUuid")
	private String orderUuid;

	@SerializedName("OrderType")
	private String orderType;

	@SerializedName("Price")
	private double price;

	@SerializedName("Limit")
	private double limit;

	@SerializedName("PricePerUnit")
	private Object pricePerUnit;

	@SerializedName("ImmediateOrCancel")
	private boolean immediateOrCancel;

	@SerializedName("QuantityRemaining")
	private double quantityRemaining;

	public void setCondition(Object condition){
		this.condition = condition;
	}

	public Object getCondition(){
		return condition;
	}

	public void setCommission(double commission){
		this.commission = commission;
	}

	public double getCommission(){
		return commission;
	}

	public void setQuantity(double quantity){
		this.quantity = quantity;
	}

	public double getQuantity(){
		return quantity;
	}

	public void setIsConditional(boolean isConditional){
		this.isConditional = isConditional;
	}

	public boolean isIsConditional(){
		return isConditional;
	}

	public void setTimeStamp(String timeStamp){
		this.timeStamp = timeStamp;
	}

	public String getTimeStamp(){
		return timeStamp;
	}

	public void setConditionTarget(Object conditionTarget){
		this.conditionTarget = conditionTarget;
	}

	public Object getConditionTarget(){
		return conditionTarget;
	}

	public void setExchange(String exchange){
		this.exchange = exchange;
	}

	public String getExchange(){
		return exchange;
	}

	public void setOrderUuid(String orderUuid){
		this.orderUuid = orderUuid;
	}

	public String getOrderUuid(){
		return orderUuid;
	}

	public void setOrderType(String orderType){
		this.orderType = orderType;
	}

	public String getOrderType(){
		return orderType;
	}

	public void setPrice(double price){
		this.price = price;
	}

	public double getPrice(){
		return price;
	}

	public void setLimit(double limit){
		this.limit = limit;
	}

	public double getLimit(){
		return limit;
	}

	public void setPricePerUnit(Object pricePerUnit){
		this.pricePerUnit = pricePerUnit;
	}

	public Object getPricePerUnit(){
		return pricePerUnit;
	}

	public void setImmediateOrCancel(boolean immediateOrCancel){
		this.immediateOrCancel = immediateOrCancel;
	}

	public boolean isImmediateOrCancel(){
		return immediateOrCancel;
	}

	public void setQuantityRemaining(double quantityRemaining){
		this.quantityRemaining = quantityRemaining;
	}

	public double getQuantityRemaining(){
		return quantityRemaining;
	}
}