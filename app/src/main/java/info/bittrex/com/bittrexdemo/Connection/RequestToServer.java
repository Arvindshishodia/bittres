package info.bittrex.com.bittrexdemo.Connection;

import android.os.AsyncTask;
import android.util.Log;


import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Bishnu on 10/26/2015.
 */
public class RequestToServer extends AsyncTask<String,String,String> {

    public static String request_json = "";
    private static RequestToServer requestToServer = null;
    public AsyncResponse delegate=null;

    public RequestToServer(String request_json1) {
        request_json = request_json1;
    }
    @Override
    protected String doInBackground(String... urls) {
        String return_response = "";
        try {
            for (String url : urls) {
                OkHttpClient client ;//= new OkHttpClient();
               // client.setConnectTimeout(30, TimeUnit.SECONDS); // connect timeout
               // client.setReadTimeout(30, TimeUnit.SECONDS);    // socket timeout
                //private final OkHttpClient client;
                client=new OkHttpClient.Builder()
                        .connectTimeout(100, TimeUnit.SECONDS)
                        .writeTimeout(100, TimeUnit.SECONDS)
                        .readTimeout(300, TimeUnit.SECONDS)
                        .build();
/*
                public ConfigureTimeouts() throws Exception {
                    client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();
                }

                public void run() throws Exception {
                    Request request = new Request.Builder()
                            .url("http://httpbin.org/delay/2") // This URL is served with a 2 second delay.
                            .build();

                    Response response = client.newCall(request).execute();
                    System.out.println("Response completed: " + response);
                }*/
                MediaType mediaType = MediaType.parse("application/octet-stream");
                RequestBody body = RequestBody.create(mediaType, request_json);
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .addHeader("cache-control", "no-cache")
                        .build();

                Response response = client.newCall(request).execute();
              /*  Log.e("firstLog",""+response.body().string());
                Log.e("secondLog",""+response.body().string().replaceAll("\"null\",","n/a"));*/
                return response.body().string().replaceAll("\"null\",","n/a");
            }
        }
        catch (Exception e)
        {
            Log.d("request error",e.getMessage());
            return "";
        }
        return return_response;
    }
    @Override
    protected void onPostExecute(String result) {
        try{
                delegate.processFinished(result);
            }
        catch(Exception e) {
            e.printStackTrace();
        }

    }
}
