package info.bittrex.com.bittrexdemo.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;

import java.security.Provider;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import info.bittrex.com.bittrexdemo.Connection.AsyncResponse;
import info.bittrex.com.bittrexdemo.Connection.GetRequestToServer;
import info.bittrex.com.bittrexdemo.Connection.RequestToServer;
import info.bittrex.com.bittrexdemo.model.CurrencyList;
import info.bittrex.com.bittrexdemo.model.SummaryResponse;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by arvind.kumar on 11/30/2017.
 */

//public class CurrencyService extends Service implements AsyncResponse {
public class CurrencyService extends Service {
    public final IBinder iBinder = new LocalBinder();
    private final Random mGenerator = new Random();
    String Service_Response = "";
    //CurrencyList currencyList;
    SummaryResponse summaryResponse;
    public static  AsyncResponse asyncResponse;
    @Override
    public void onCreate() {
        super.onCreate();
        summaryResponse=new SummaryResponse();
        Timer timerObj = new Timer();
        TimerTask timerTask=new TimerTask() {
            @Override
            public void run() {
                callServicess();
            }
        };
        timerObj.schedule(timerTask,0,10000);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // getDataFromThread();
        return super.onStartCommand(intent, flags, startId);
    }

    public void callServicess() {
        GetRequestToServer getRequestToServer = new GetRequestToServer();
        getRequestToServer.delegate = asyncResponse;
       // getRequestToServer.execute("https://bittrex.com/api/v1.1/public/getmarkets");
        getRequestToServer.execute("https://bittrex.com/api/v1.1/public/getmarketsummaries");
    }

    @Override
    public IBinder onBind(Intent intent) {
        return iBinder;
    }


    public void processFinished(String data) {
        Service_Response = data;
        Gson gson=new Gson();
        summaryResponse=gson.fromJson(data,SummaryResponse.class);
    }

    public class LocalBinder extends Binder {
        public CurrencyService getService() {
            return CurrencyService.this;
        }
    }

    public void getDataFromThread() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    threadMsg(callService(""));
                } catch (Exception e) {

                }
            }

            private void threadMsg(String msg) {

                if (!msg.equals(null) && !msg.equals("")) {
                    Message msgObj = handler.obtainMessage();
                    Bundle b = new Bundle();
                    b.putString("message", msg);
                    msgObj.setData(b);
                    handler.sendMessage(msgObj);
                }
            }

            private final Handler handler = new Handler() {

                public void handleMessage(Message msg) {

                    String aResponse = msg.getData().getString("message");

                    if ((null != aResponse)) {

                        // ALERT MESSAGE
                        Toast.makeText(
                                getBaseContext(),
                                "Server Response: " + aResponse,
                                Toast.LENGTH_SHORT).show();
                    } else {

                        // ALERT MESSAGE
                        Toast.makeText(
                                getBaseContext(),
                                "Not Got Response From Server.",
                                Toast.LENGTH_SHORT).show();
                    }

                }
            };
        });
        thread.start();
    }

    public SummaryResponse getService_Response() {
        return summaryResponse;
    }

    public String callService(String url) {
        String return_response = "";
        try {

            OkHttpClient client;//= new OkHttpClient();
            // client.setConnectTimeout(30, TimeUnit.SECONDS); // connect timeout
            // client.setReadTimeout(30, TimeUnit.SECONDS);    // socket timeout
            //private final OkHttpClient client;
            client = new OkHttpClient.Builder()
                    .connectTimeout(100, TimeUnit.SECONDS)
                    .writeTimeout(100, TimeUnit.SECONDS)
                    .readTimeout(300, TimeUnit.SECONDS)
                    .build();
/*
                public ConfigureTimeouts() throws Exception {
                    client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();
                }

                public void run() throws Exception {
                    Request request = new Request.Builder()
                            .url("http://httpbin.org/delay/2") // This URL is served with a 2 second delay.
                            .build();

                    Response response = client.newCall(request).execute();
                    System.out.println("Response completed: " + response);
                }*/
            MediaType mediaType = MediaType.parse("application/octet-stream");
            RequestBody body = RequestBody.create(mediaType, url);
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .addHeader("cache-control", "no-cache")
                    .build();

            Response response = client.newCall(request).execute();
              /*  Log.e("firstLog",""+response.body().string());
                Log.e("secondLog",""+response.body().string().replaceAll("\"null\",","n/a"));*/
            return response.body().string().replaceAll("\"null\",", "n/a");

        } catch (Exception e) {
            Log.d("request error", e.getMessage());
            return "";
        }

    }
}
