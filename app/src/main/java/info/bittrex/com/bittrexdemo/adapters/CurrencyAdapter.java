package info.bittrex.com.bittrexdemo.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.corycharlton.bittrexapi.BittrexApiClient;
import com.corycharlton.bittrexapi.request.GetOrderHistoryRequest;
import com.corycharlton.bittrexapi.request.PlaceBuyLimitOrderRequest;
import com.corycharlton.bittrexapi.request.PlaceSellLimitOrderRequest;
import com.corycharlton.bittrexapi.request.Request;
import com.corycharlton.bittrexapi.response.GetMarketsResponse;
import com.corycharlton.bittrexapi.response.PlaceBuyLimitOrderResponse;
import com.corycharlton.bittrexapi.response.PlaceSellLimitOrderResponse;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;

import info.bittrex.com.bittrexdemo.R;
import info.bittrex.com.bittrexdemo.fragments.OrderHistoryFragment;
import info.bittrex.com.bittrexdemo.model.SummaryResult;
import info.bittrex.com.bittrexdemo.settings.ApplicationSettings;
import info.bittrex.com.bittrexdemo.utils.Log;
import info.bittrex.com.bittrexdemo.utils.OkHttpDownloader;
import info.bittrex.com.bittrexdemo.utils.ToastCallback;

/**
 * Created by arvind.kumar on 11/30/2017.
 */

public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.ViewHolder> {
    private List<SummaryResult> summaryResults;
    Context mContext;
    public final int BUY = 1;
    public final int SALE = 0;
    EditText edt_amount;
    EditText edt_curreny;
    EditText edt_number;


    public CurrencyAdapter() {

    }

    public CurrencyAdapter(Context context, List<SummaryResult> summaryResults) {

        this.mContext = context;
        this.summaryResults = summaryResults;

    }

    @Override
    public CurrencyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.currency_item, parent, false);

        CurrencyAdapter.ViewHolder viewHolder = new CurrencyAdapter.ViewHolder(view);

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(CurrencyAdapter.ViewHolder holder, int position) {
        final SummaryResult result = summaryResults.get(position);
        holder.txt_mktName.setText(result.getMarketName());
        holder.txt_basevolume.setText("" + result.getBaseVolume());
        Double last = result.getLast();
        holder.txt_lastprice.setText("" + String.format("%f", last));
        double percentChange = (result.getLast() - result.getPrevDay()) * 100 / result.getPrevDay();
        percentChange = Double.parseDouble(new DecimalFormat("###.##").format(percentChange));
        holder.txt_change.setText("" + percentChange + " %");
        if (percentChange >= 0) {
            holder.txt_change.setTextColor(mContext.getResources().getColor(R.color.green));
        } else {
            holder.txt_change.setTextColor(mContext.getResources().getColor(R.color.red));
        }
        holder.img_buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlert(true, result.getMarketName());
            }
        });
        holder.img_sale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlert(false, result.getMarketName());
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemCount() {
        if (summaryResults == null)
            return 0;
        else {
            return summaryResults.size();
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_mktName, txt_basevolume, txt_lastprice, txt_change;
        private ImageView img_sale, img_buy;

        public ViewHolder(View itemView) {
            super(itemView);

            txt_mktName = (TextView) itemView.findViewById(R.id.txt_mktName);
            txt_basevolume = (TextView) itemView.findViewById(R.id.txt_basevolume);
            txt_lastprice = (TextView) itemView.findViewById(R.id.txt_lastprice);
            txt_change = (TextView) itemView.findViewById(R.id.txt_change);
            img_buy = (ImageView) itemView.findViewById(R.id.img_buy);
            img_sale = (ImageView) itemView.findViewById(R.id.img_sale);
        }


    }

    public void showAlert(final Boolean isBUY, final String name) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View dialogView = inflater.inflate(R.layout.alert_buy_sale, null);
        dialogBuilder.setView(dialogView);

        edt_amount = (EditText) dialogView.findViewById(R.id.edt_amount);
        edt_curreny = (EditText) dialogView.findViewById(R.id.edt_curreny);
        edt_number = (EditText) dialogView.findViewById(R.id.edt_number);
        edt_curreny.setText(name);

        //dialogBuilder.setTitle("Enters Data");
        if (isBUY) {
            dialogBuilder.setMessage("Enter data for buy");
        } else {
            dialogBuilder.setMessage("Enter data for sale");
        }
        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();
                if (!edt_amount.getText().toString().trim().equalsIgnoreCase("") &&
                        !edt_number.getText().toString().trim().equalsIgnoreCase("")) {
                    double amount = Double.parseDouble(edt_amount.getText().toString());
                    double unit = Double.parseDouble(edt_number.getText().toString());

                    if (isBUY) {
                        buyOrder(name, unit, amount);
                    } else {
                        sellOrder(name, unit, amount);
                    }
                }
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass


            }
        });
        AlertDialog b = dialogBuilder.create();
        b.requestWindowFeature(Window.FEATURE_NO_TITLE);
        b.show();
    }


    void buyOrder(String name, double unit, double rate) {
        BittrexApiClient client = new BittrexApiClient.Builder()
                .downloader(new OkHttpDownloader())
                .key(ApplicationSettings.instance().getKey())
                .secret(ApplicationSettings.instance().getSecret())
                .build();

        client.executeAsync(new PlaceBuyLimitOrderRequest("BTC-XVG", unit, rate),
                new BuyResponse(mContext));
    }


    void sellOrder(String name, double unit, double rate) {
        BittrexApiClient client = new BittrexApiClient.Builder()
                .downloader(new OkHttpDownloader())
                .key(ApplicationSettings.instance().getKey())
                .secret(ApplicationSettings.instance().getSecret())
                .build();

        client.executeAsync(new PlaceSellLimitOrderRequest("BTC-XVG", unit, rate),
                new SellResponse(mContext));
    }


    private class BuyResponse extends ToastCallback<PlaceBuyLimitOrderResponse> {
        BuyResponse(@NonNull Context context) {
            super(context);
        }

        @Override
        public void onFailure(Request<PlaceBuyLimitOrderResponse> request, IOException e) {
            super.onFailure(request, e);
            Toast.makeText(mContext,""+e,Toast.LENGTH_LONG).show();
            Log.d("RESPONSE",""+e);
        }

        @Override
        public void onResponse(Request<PlaceBuyLimitOrderResponse> request, PlaceBuyLimitOrderResponse response) {
            super.onResponse(request, response);
            Toast.makeText(mContext,response.toString(),Toast.LENGTH_LONG).show();
            Log.d("RESPONSE",response.toString());
            if(response.success()){
                Toast.makeText(mContext,"Order Placed",Toast.LENGTH_LONG).show();
            }
        }
    }


    private class SellResponse extends ToastCallback<PlaceSellLimitOrderResponse> {
        SellResponse(@NonNull Context context) {
            super(context);
        }

        @Override
        public void onFailure(Request<PlaceSellLimitOrderResponse> request, IOException e) {
            super.onFailure(request, e);
        }

        @Override
        public void onResponse(Request<PlaceSellLimitOrderResponse> request, PlaceSellLimitOrderResponse response) {
            super.onResponse(request, response);
            if(response.success()){
                Toast.makeText(mContext,"Order Placed",Toast.LENGTH_LONG).show();
            }

        }
    }
}
