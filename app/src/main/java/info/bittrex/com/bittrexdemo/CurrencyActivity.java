package info.bittrex.com.bittrexdemo;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import info.bittrex.com.bittrexdemo.Connection.AsyncResponse;
import info.bittrex.com.bittrexdemo.adapters.CurrencyAdapter;
import info.bittrex.com.bittrexdemo.model.CurrencyList;
import info.bittrex.com.bittrexdemo.model.SummaryResponse;
import info.bittrex.com.bittrexdemo.services.CurrencyService;
import info.bittrex.com.bittrexdemo.services.CurrencyService.LocalBinder;

public class CurrencyActivity extends AppCompatActivity implements AsyncResponse {
    CurrencyService currencyService;
    boolean mBound = false;
    private RecyclerView recycle_currency;
    private TextView txt_data;
    SummaryResponse summaryResponse=new SummaryResponse();
    CurrencyAdapter currencyAdapter;
    RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency);
        recycle_currency = (RecyclerView) findViewById(R.id.recycle_currency);
        txt_data = (TextView) findViewById(R.id.txt_data);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());

        txt_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //     txt_data.setText(""+currencyService.getService_Response());
                summaryResponse = currencyService.getService_Response();
               // currencyAdapter = new CurrencyAdapter(CurrencyActivity.this, summaryResponse);
                recycle_currency.setLayoutManager(mLayoutManager);
                recycle_currency.setAdapter(currencyAdapter);
                //   Toast.makeText(CurrencyActivity.this,""+currencyService.getService_Response(),Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        CurrencyService.asyncResponse = CurrencyActivity.this;
        Intent intent = new Intent(this, CurrencyService.class);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(serviceConnection);
        mBound = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Intent intent = new Intent(this, CurrencyService.class);
        stopService(intent);
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LocalBinder binder = (LocalBinder) service;
            currencyService = binder.getService();
            txt_data.setText("service connected");
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
        }
    };

    @Override
    public void processFinished(String data) {
        Gson gson = new Gson();
        summaryResponse = gson.fromJson(data, SummaryResponse.class);
        //currencyAdapter = new CurrencyAdapter(CurrencyActivity.this, summaryResponse);
        recycle_currency.setLayoutManager(mLayoutManager);
        recycle_currency.setAdapter(currencyAdapter);
        currencyAdapter.notifyDataSetChanged();
    }
}
