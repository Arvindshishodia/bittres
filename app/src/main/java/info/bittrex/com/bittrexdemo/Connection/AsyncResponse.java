package info.bittrex.com.bittrexdemo.Connection;

/**
 * Created by Upadhyays on 25-04-2016.
 */
public interface AsyncResponse {
    void processFinished(String data);
}
