package info.bittrex.com.bittrexdemo.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class OrderHistoryPOJO{

	@SerializedName("result")
	private List<ResultItem> result;

	@SerializedName("success")
	private boolean success;

	@SerializedName("message")
	private String message;

	public void setResult(List<ResultItem> result){
		this.result = result;
	}

	public List<ResultItem> getResult(){
		return result;
	}

	public void setSuccess(boolean success){
		this.success = success;
	}

	public boolean isSuccess(){
		return success;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}
}