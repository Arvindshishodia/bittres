package info.bittrex.com.bittrexdemo.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.corycharlton.bittrexapi.BittrexApiClient;
import com.corycharlton.bittrexapi.model.OrderHistory;
import com.corycharlton.bittrexapi.request.GetOrderHistoryRequest;
import com.corycharlton.bittrexapi.request.Request;
import com.corycharlton.bittrexapi.response.GetDepositHistoryResponse;
import com.corycharlton.bittrexapi.response.GetMarketHistoryResponse;
import com.corycharlton.bittrexapi.response.GetMarketsResponse;
import com.corycharlton.bittrexapi.response.GetOpenOrdersResponse;
import com.corycharlton.bittrexapi.response.GetOrderBookResponse;
import com.corycharlton.bittrexapi.response.GetOrderHistoryResponse;
import com.corycharlton.bittrexapi.response.GetWithdrawalHistoryResponse;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;

import info.bittrex.com.bittrexdemo.R;
import info.bittrex.com.bittrexdemo.adapters.HistoryAdapter;
import info.bittrex.com.bittrexdemo.model.SummaryResponse;
import info.bittrex.com.bittrexdemo.settings.ApplicationSettings;
import info.bittrex.com.bittrexdemo.utils.Log;
import info.bittrex.com.bittrexdemo.utils.OkHttpDownloader;
import info.bittrex.com.bittrexdemo.utils.ToastCallback;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderHistoryFragment extends Fragment {


    private RecyclerView rec_history;
    private ArrayList<OrderHistory> orderHistory;
    private HistoryAdapter adapter;
    private Context context;
    SummaryResponse summaryResponse = new SummaryResponse();

    public OrderHistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View  view =  inflater.inflate(R.layout.fragment_order_history, container, false);
        rec_history = (RecyclerView) view.findViewById(R.id.rec_history);
        context = getActivity();
        rec_history.setLayoutManager(new LinearLayoutManager(getActivity()));
        getOrderHIstory();
        return view;
    }

    void getOrderHIstory(){
         BittrexApiClient client = new BittrexApiClient.Builder()
                .downloader(new OkHttpDownloader())
                .key(ApplicationSettings.instance().getKey())
                .secret(ApplicationSettings.instance().getSecret())
                .build();

         // GetOrderHistoryRequest   // For OPEN ORDERS
        //GetOrderBookRequest
        // GetOpenOrdersRequest

        // GetMarketsRequest for Market details

      client.executeAsync(new GetOrderHistoryRequest(),
                new OrderHistoryAsyn(getActivity().getBaseContext()));
    }


   /* private class OpenO extends ToastCallback<GetOrderHistoryResponse> {
        OpenO(@NonNull Context context) {
            super(context);
        }

        @Override
        public void onFailure(Request<GetOrderHistoryResponse> request, IOException e) {
            super.onFailure(request, e);
        }

        @Override
        public void onResponse(Request<GetMarketsResponse> request, GetOrderHistoryResponse response) {
            super.onResponse(request, response);
            Log.d("RESPONSE D",response.toString());

        }
    }*/

    //    Log.d("RESPONSE D",response.toString());

    private class OrderHistoryAsyn extends ToastCallback<GetOrderHistoryResponse> {
        OrderHistoryAsyn(@NonNull Context context) {
            super(context);
        }

        @Override
        public void onFailure(Request<GetOrderHistoryResponse> request, IOException e) {
            super.onFailure(request, e);
        }

        @Override
        public void onResponse(Request<GetOrderHistoryResponse> request, GetOrderHistoryResponse response) {
            super.onResponse(request, response);
           orderHistory = response.result();
           if(response.success()){
                adapter = new HistoryAdapter(context,orderHistory);
               rec_history.setAdapter(adapter);
           }
           
        }
    }



}
