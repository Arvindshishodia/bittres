package info.bittrex.com.bittrexdemo.Connection;

import android.os.AsyncTask;
import android.util.Log;


import java.net.ConnectException;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


/**
 * Created by Upadhyays on 25-04-2016.
 */
public class GetRequestToServer extends AsyncTask<String,String,String> {

    public AsyncResponse delegate=null;
    public String RequestUrl="";
    @Override
    protected String doInBackground(String... urls) {
        String return_response = "";
        try {
            for (String url : urls) {
                Log.e("request url",""+url);
                RequestUrl=""+url;
                OkHttpClient.Builder b = new OkHttpClient.Builder();
                b.readTimeout(40, TimeUnit.SECONDS);
                b.writeTimeout(40, TimeUnit.SECONDS);
                OkHttpClient client = b.build();
                MediaType mediaType = MediaType.parse("application/octet-stream");
                Request request = new Request.Builder()
                        .url(url)
                        .addHeader("cache-control", "no-cache")
                        .build();

                Response response = client.newCall(request).execute();
                return_response= response.body().string();
            }
        }
        catch (ConnectException e){
            return "Login Timeout Exception ";
        }
        catch (Exception e)
        {
            return "";
        }
        return return_response;
    }
    @Override
    protected void onPostExecute(String result) {
        try{

            delegate.processFinished(result);
        }
        catch(Exception e) {
            e.printStackTrace();
        }

    }

}
