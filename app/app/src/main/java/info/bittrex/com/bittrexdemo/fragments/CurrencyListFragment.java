package info.bittrex.com.bittrexdemo.fragments;


import android.app.Fragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;

import info.bittrex.com.bittrexdemo.Connection.AsyncResponse;
import info.bittrex.com.bittrexdemo.CurrencyActivity;
import info.bittrex.com.bittrexdemo.R;
import info.bittrex.com.bittrexdemo.adapters.CurrencyAdapter;
import info.bittrex.com.bittrexdemo.model.SummaryResponse;
import info.bittrex.com.bittrexdemo.services.CurrencyService;

/**
 * A simple {@link Fragment} subclass.
 */
public class CurrencyListFragment extends Fragment  implements AsyncResponse {
    CurrencyService currencyService;
    boolean mBound = false;
    private RecyclerView recycle_currency;
    SummaryResponse summaryResponse=new SummaryResponse();
    CurrencyAdapter currencyAdapter;
    RecyclerView.LayoutManager mLayoutManager;

    public CurrencyListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View view=inflater.inflate(R.layout.fragment_currency_list, container, false);
        recycle_currency=(RecyclerView)view.findViewById(R.id.recycle_currency);
        mLayoutManager = new LinearLayoutManager(getActivity());
       return view;
    }
    @Override
    public void onStart() {
        super.onStart();
        CurrencyService.asyncResponse = this;
        Intent intent = new Intent(getActivity(), CurrencyService.class);
        getActivity().bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void processFinished(String data) {
        Gson gson = new Gson();
        summaryResponse = gson.fromJson(data, SummaryResponse.class);
        currencyAdapter = new CurrencyAdapter(getActivity(), summaryResponse);
        recycle_currency.setLayoutManager(mLayoutManager);
        recycle_currency.setAdapter(currencyAdapter);
        currencyAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Intent intent = new Intent(getActivity(), CurrencyService.class);
        getActivity().stopService(intent);
    }
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            CurrencyService.LocalBinder binder = (CurrencyService.LocalBinder) service;
            currencyService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
        }
    };
}
